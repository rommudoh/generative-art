(ns generative-art.perlin-graph)

(ns noise-graph.core
  (:require [quil.core :as q]))

(defn draw-state []
  (q/background 0)
  (q/no-fill)
  (q/stroke 255)
  (q/begin-shape)
  (doseq [x (range (q/width))
          :let [x' (q/map-range x 0 (q/width) 0 10)
                y (* (q/height) (q/noise x'))]]
    (q/curve-vertex x y))
  (q/end-shape))

(q/defsketch perlin-graph
  :title "Graphing the Perlin Noise"
  :size [500 500]
  :draw draw-state)
