(ns generative-art.random-counts
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 30)
  {:counts (vec (repeat 20 0))})

(defn update-state [state]
  (-> state
     (update-in [:counts (rand-int (count (:counts state)))] inc)))

(defn draw-state [{:keys [counts]}]
  (q/background 255)
  (q/stroke 0)
  (q/fill 175)
  (let [bars (count counts)
        bar-width (/ (q/width) bars)]
    (doseq [bar (range (count counts))
            :let [bar-height (get counts bar)]]
      (q/rect (* bar bar-width) (- (q/height) bar-height)
              (dec bar-width) bar-height))))

(q/defsketch random-counts
  :title "Random Counts"
  :size [500 200]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])

