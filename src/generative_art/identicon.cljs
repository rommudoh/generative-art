(ns generative-art.identicon
  (:require [quil.core :as q :include-macros true]
            [goog.dom :as dom]
            [goog.crypt :as crypt])
  (:import goog.crypt.Sha256))

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb))

(defn str->hash [s]
  (let [byte-array (crypt/stringToUtf8ByteArray s)
        sha (Sha256.)]
    (.update sha byte-array)
    (.digest sha)))

(defn mirror [coll repeat-middle?]
  (if repeat-middle?
    (concat coll (reverse coll))
    (concat coll (rest (reverse coll)))))

(defn draw-identicon [identicon]
  (let [rows (count identicon)
        cols (count (first identicon))
        height (/ (q/height) rows)
        width (/ (q/width) cols)]
    (doseq [row (range rows)
            col (range cols)
            :let [line (nth identicon row)
                  color (nth line col)]
            :when (some? color)]
      (q/fill color 255 255)
      (q/rect (* col width) (* row height) width height))))

(defn generate-identicon [s width height]
  (let [hash (str->hash s)
        [color & rest] hash
        unique-width (quot (inc width) 2)
        square-count (* height unique-width)]
    (->> (take square-count rest)
         (map even?)
         (map #(when % color))
         (partition unique-width)
         (map #(mirror % (even? width))))))

(defn draw-default-text []
  (q/fill 0)
  (q/text-size 20)
  (let [text "Please input something!"
        width (q/text-width text)]
    (q/text text
            (/ (- (q/width) width) 2)
            (/ (q/height) 2))))

(defn draw []
  (q/background 240)
  (let [input (.-value (dom/getElement "identicon-input"))]
    (if (empty? input)
      (draw-default-text)
      (draw-identicon (generate-identicon input 5 5)))))

(defn ^:export run-sketch []
  (q/defsketch identicon
    :host "identicon"
    :size [500 500]
    :setup setup
    :draw draw))
