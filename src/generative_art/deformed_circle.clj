(ns generative-art.deformed-circle
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 30)
  (q/background 255)
  (q/stroke 0 15)
  (q/no-fill)
  {:t 0})

(defn update-state [state]
  (update state :t inc))

(defn draw-state [{:keys [t]}]
  (when (= 0 (mod (q/frame-count) 600))
    (q/background 255))
  (q/with-translation [(/ (q/width) 2)
                       (/ (q/height) 2)]
    (q/begin-shape)
    (doseq [i (range 200)
            :let [ang (q/map-range i 0 200 0 (* 2 (Math/PI)))
                  rad (* 200 (q/noise (* i 0.01) (* t 0.005)))
                  x (* rad (q/cos ang))
                  y (* rad (q/sin ang))]]
      (q/curve-vertex x y))
    (q/end-shape :close)))

(q/defsketch deformed-circle
  :title "Deformed Circle"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
