(ns generative-art.gauss-random
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 30)
  (q/background 255)
  {:generator (java.util.Random.)
   :num 0})

(defn update-state [{:keys [generator] :as state}]
  (assoc state :num (.nextGaussian generator)))

(defn draw-state [{:keys [num]}]
  (let [sd 60
        mean 320
        x (+ (* sd num) mean)]
    (q/no-stroke)
    (q/fill 0 10)
    (q/ellipse x 180 16 16)))

(q/defsketch gauss-random
  :title "Gaussian Randomness"
  :size [640 320]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
