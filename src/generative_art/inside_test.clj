(ns generative-art.inside-test
  (:require [quil.core :as q]))

(def polygons
  [{:points [{:x 120 :y 120}
             {:x 160 :y 120}
             {:x 140 :y 160}
             {:x 100 :y 160}]
    :hover-color [255 0 0]
    :normal-color [255 255 255]}
   {:points [{:x 220 :y 310}
             {:x 250 :y 280}
             {:x 240 :y 260}
             {:x 280 :y 210}
             {:x 300 :y 240}
             {:x 286 :y 260}
             {:x 310 :y 280}
             {:x 320 :y 316}]
    :hover-color [0 255 0]
    :normal-color [255 255 0]}])

(defn setup []
  (q/frame-rate 30))

(defn inside-shape? [points [x y]]
  (loop [i 0 inside? false]
    (let [{x1 :x y1 :y} (get points i)
          {x2 :x y2 :y} (get points (mod (inc i) (count points)))]
      (if (= i (count points))
        inside?
        (let [below-y1? (> y1 y)
              below-y2? (> y2 y)
              within-ys-edge? (not= below-y1? below-y2?)]
          (if within-ys-edge?
            (let [slope-of-line (/ (- x2 x1)
                                   (- y2 y1))
                  point-on-line (+ (* slope-of-line
                                      (- y y1))
                                   x1)
                  left-of-line? (< x point-on-line)]
              (if left-of-line?
                (recur (inc i) (not inside?))
                (recur (inc i) inside?)))
            (recur (inc i) inside?)))))))

(defn draw []
  (q/background 0)
  (doseq [polygon polygons]
    (if (inside-shape? (:points polygon) [(q/mouse-x) (q/mouse-y)])
      (q/fill (:hover-color polygon))
      (q/fill (:normal-color polygon)))
    (q/begin-shape)
    (doseq [{:keys [x y]} (:points polygon)]
      (q/vertex x y))
    (q/end-shape :close)))

(q/defsketch inside-test
  :title "Inside Test"
  :size [500 500]
  :setup setup
  :draw draw)
