(ns generative-art.random-walker
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 30)
  (q/background 255)
  {:walker {:x (/ (q/width) 2)
            :y (/ (q/height) 2)}
   :gauss {:rnd (java.util.Random.)
           :std 2
           :mean 5}})

(defn update-walker [walker {:keys [rnd std mean]}]
  (let [step-size (+ (* std (.nextGaussian rnd)) mean)
        x-step (- (rand (* 2 step-size)) step-size)
        y-step (- (rand (* 2 step-size)) step-size)]
    (-> walker
        (update :x + x-step)
        (update :y + y-step))))

(defn update-state [{:keys [gauss] :as state}]
  (-> state
      (update :walker #(update-walker % gauss))))

(defn draw-walker [{:keys [x y]}]
  (q/stroke 0)
  (q/point x y))

(defn draw-state [{:keys [walker]}]
  (draw-walker walker))

(q/defsketch random-walker
  :title "Random Walker"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
