(ns generative-art.cljs-input
  (:require [quil.core :as q :include-macros true]
            [goog.dom :as dom]))

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb))

(defn draw []
  (q/background 240)
  (q/fill 0)
  (let [input (.-value (dom/getElement "input"))]
    (q/text input (/ (q/width) 2) (/ (q/height) 2))))

(defn ^:export run-sketch []
  (q/defsketch cljs-input
    :host "cljs-input"
    :size [500 500]
    :setup setup
    :draw draw))

