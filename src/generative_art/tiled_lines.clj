(ns generative-art.tiled-lines
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def default-width 32)
(def default-height 32)
(def max-seed 200000)

(defn grid [{:keys [width height]}]
  (q/stroke 255 0 0)
  (q/no-fill)
  (q/rect 0 0 width height))

(defn rects [{:keys [width height]}]
  (q/no-stroke)
  (q/fill (rand-int 255))
  (q/rect 0 0 width height))

(defn random-lines [{:keys [width height]}]
  (q/stroke 0)
  (if (< (q/random 2) 1)
    (q/line 0 0 width height)
    (q/line 0 height width 0)))

(defn random-curves [{:keys [width height]}]
  (q/stroke 0)
  (q/no-fill)
  (if (< (q/random 2) 1)
    (do (q/arc 0 0 width height 0 q/HALF-PI)
        (q/arc width height width height q/PI (+ q/PI q/HALF-PI)))
    (do (q/arc width 0 width height q/HALF-PI q/PI)
        (q/arc 0 height width height (+ q/PI q/HALF-PI) q/TWO-PI))))

(defn setup []
  (q/frame-rate 30)
  {:width default-width
   :height default-height
   :fns [rects random-lines random-curves]
   :fn 1
   :seed (rand-int max-seed)
   :grid? false})

(defn draw-state [{:keys [width height fns fn seed grid?] :as state}]
  (q/random-seed seed)
  (q/background 255)
  ;; draw tiled function
  (doseq [x (range 0 (q/width) width)
          y (range 0 (q/height) height)]
    (q/with-translation [x y]
      (when grid? (grid state))
      ((get fns fn) state))))

(defn update-state [state]
  (if (q/key-pressed?)
    (case (q/key-as-keyword)
      :+ (-> state
            (update :width dec)
            (update :height dec))
      :- (-> state
            (update :width inc)
            (update :height inc))
      :r (assoc state :seed (rand-int max-seed))
      :x (update state :width inc)
      :X (update state :width dec)
      :y (update state :height inc)
      :Y (update state :height dec)
      := (assoc state :width (:height state))
      :f (assoc state :fn (mod (inc (:fn state))
                               (count (:fns state))))
      :1 (assoc state :fn 1)
      :2 (assoc state :fn 2)
      :g (update state :grid? not)
      state)
    state))

(q/defsketch tiled-lines
  :title "Tiled Lines"
  :size [640 640]
  :setup setup
  :draw draw-state
  :update update-state
  :middleware [m/fun-mode])
