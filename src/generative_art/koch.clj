(ns generative-art.koch
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def max-i 5)

(defn deg->rad [x]
  (/ (* q/PI x) 180))

(defn setup []
  (q/frame-rate 30)
  (q/text-size 20)
  {:i 0
   :delta (deg->rad 60)
   :curve '(:forward)})

(defn expand [curve]
  (mapcat (fn [x]
            (case x
              :forward '(:forward :rotate-left :forward :rotate-right :rotate-right :forward :rotate-left :forward)
              (list x)))
          curve))

(defn update-state [state]
  (if (= 0 (mod (q/frame-count) 30))
    (if (< (:i state) max-i)
      (-> state
         (update :i inc)
         (update :curve expand))
      (setup))
    state))

(defn draw-state [state]
  (q/background 0)
  (q/fill 255)
  (q/text (str "Iteration: " (:i state)) 100 100)
  (q/no-fill)
  (q/stroke 255)
  (let [d (/ (q/width) (Math/pow 3.0 (:i state)))]
    (loop [x 0
           y (/ (q/width) 2)
           alpha 0
           [instr & tail] (:curve state)]
      (when instr
        (case instr
          :forward (let [x' (+ x (* d (q/cos alpha)))
                         y' (+ y (* d (q/sin alpha)))]
                     (q/line x y x' y')
                     (recur x' y' alpha tail))
          :rotate-left (recur x y (+ alpha (:delta state)) tail)
          :rotate-right (recur x y (- alpha (:delta state)) tail))))))

(q/defsketch koch
  :title "Koch Curve"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
