(ns generative-art.perlin-jumping-ball
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  ; Set frame rate to 30 frames per second.
  (q/frame-rate 30)
  (q/text-size 16)
  {:t 0
   :t-increment 0})

(defn update-state [state]
  ; Update sketch state by changing circle color and position.
  {:t-increment (q/map-range (q/mouse-x) 0 (q/width) 0 0.2)
   :t (+ (:t state) (:t-increment state))})

(defn draw-state [{:keys [t t-increment]}]
  (q/background 0)
  (let [noise (q/noise t)
        y (* (q/height) noise)]
    (q/fill 0 255 0)
    (q/ellipse (/ (q/width) 2) y 60 60)
    (q/fill 255)
    (q/text (str "t-increment = " t-increment) 20 30)
    (q/text (str "t = " t) 20 60)
    (q/text (str "noise(t) = " noise) 20 90)
    (q/text (str "height * noise(t) = " y) 20 120)))

(q/defsketch perlin-jumping-ball
  :title "Manipulating the increment with Mouse"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
