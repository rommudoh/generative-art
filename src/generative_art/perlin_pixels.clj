(ns generative-art.perlin-pixels
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def noise-increment 0.01)

(defn setup []
  (q/frame-rate 30)
  (q/no-stroke)
  (q/noise-detail 8 0.7)
  {:t 0})

(defn update-state [state]
  (update state :t + noise-increment))

(defn draw-state [{:keys [t]}]
  (let [px (q/pixels)]
    (doseq [x (range 0 (q/width))
            y (range 0 (q/height))
            :let [r (q/map-range (q/noise (* noise-increment x) (* noise-increment y) t) 0 1 0 255)
                  g (q/map-range (q/noise (+ 10000 (* noise-increment x)) (* noise-increment y) t) 0 1 0 255)
                  b (q/map-range (q/noise (* noise-increment x) (+ 10000 (* noise-increment y)) t) 0 1 0 255)]]
      (aset-int px (+ x (* y (q/width))) (q/color r g b))))
  (q/update-pixels)
  (q/fill 0)
  (q/text (str "t = " t) 10 30))

(q/defsketch perlin-pixels
  :title "Two-dimensional Perlin Noise in pixels"
  :size [300 200]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
