(ns generative-art.perlin-landscape
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def noise-increment 0.01)

(defn setup []
  (q/frame-rate 30)
  (q/no-stroke)
  {:t 0})

(defn update-state [state]
  (update state :t + noise-increment))

(defn vertex [x y t]
  (q/vertex x y (q/map-range (q/noise (* noise-increment x) (* noise-increment y) t) 0 1 0 100)))

(defn draw-state [{:keys [t]}]
  (q/background 255)
  (q/camera 250 200 130 100 100 0 0 0 -1)
  (q/stroke-weight 2)
  (q/stroke 0)
  (q/fill 125)
  (doseq [x (range 0 200 10)]
    (q/begin-shape :quad-strip)
    (doseq [y (range 0 200 20)]
      (vertex x y t)
      (vertex (+ x 10) y t)
      (vertex x (+ y 10) t)
      (vertex (+ x 10) (+ y 10) t))
    (q/end-shape)))

(q/defsketch perlin-landscape
  :title "Perlin Noise as Height"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :renderer :opengl
  :middleware [m/fun-mode])
