(ns generative-art.wobbly-blob
  (:require [quil.core :as q :include-macros true]
            [quil.middleware :as m]))

(def num-points 6)

(defn noise-step []
  (if (q/mouse-pressed?)
    0.01
    0.005))

(defn create-points []
  (let [x-offset (/ (q/width) 2)
        y-offset (/ (q/height) 2)
        radius (* 0.75 x-offset)
        angle-step (/ (* 2 Math/PI) num-points)]
    (doall
     (for [i (range num-points)]
       (let [theta (* i angle-step)
             x (+ x-offset (* radius (q/cos theta)))
             y (+ y-offset (* radius (q/sin theta)))]
         {:x x :y y :origin-x x :origin-y y
          :noise-offset-x (rand 1000)
          :noise-offset-y (rand 1000)})))))

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb)
  {:points (create-points)
   :hue 0
   :hue-noise-offset 0})

(defn update-point [{:keys [origin-x origin-y
                            noise-offset-x
                            noise-offset-y] :as point}]
  (let [nx (q/noise noise-offset-x noise-offset-x)
        ny (q/noise noise-offset-y noise-offset-y)
        x' (q/map-range nx 0 1 (- origin-x (* 0.1 (q/width))) (+ origin-x (* 0.1 (q/width))))
        y' (q/map-range ny 0 1 (- origin-y (* 0.1 (q/height))) (+ origin-y (* 0.1 (q/height))))]
    (-> point
        (assoc :x x')
        (assoc :y y')
        (update :noise-offset-x #(+ % (noise-step)))
        (update :noise-offset-y #(+ % (noise-step))))))

(defn update-state [{:keys [hue-noise-offset] :as state}]
  (let [hue-noise (q/noise hue-noise-offset hue-noise-offset)
        hue (q/map-range hue-noise 0 1 0 360)]
    (-> state
        (update :points (fn [points] (map update-point points)))
        (update :hue-noise-offset #(+ % (noise-step)))
        (assoc :hue hue))))

(defn draw-state [{:keys [points hue]}]
  (q/background 0)
  (q/fill hue 255 190)
  (q/begin-shape)
  (doseq [{:keys [x y]} points]
    (q/curve-vertex x y))
  ;; repeat first three points to make curve smooth
  (doseq [{:keys [x y]} (take 3 points)]
    (q/curve-vertex x y))
  (q/end-shape))

(q/defsketch wobbly-blob
  :title "Wobbly Blob, click to accellerate"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
