(ns generative-art.random-stripes
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def num-stripes 10)

(def colors [[255   0   0] ; red
             [0 255   0] ; green
             [0   0 255] ; blue
             [255 255   0] ; yellow
             [255   0 255] ; magenta
             [0 255 255] ; cyan
             ])

(defn choose-colors []
  (repeatedly num-stripes #(rand-nth colors)))

(defn setup []
  (q/frame-rate 30)
  {:colors (choose-colors)
   :pressed? false})

(defn update-state [{:keys [pressed?] :as state}]
  (if (q/mouse-pressed?)
    (if pressed?
      state
      (-> state
          (assoc :colors (choose-colors))
          (assoc :pressed? true)))
    (assoc state :pressed? false)))

(defn draw-state [{:keys [colors]}]
  (q/background 255)
  (let [stripe-width (/ (q/width) num-stripes)]
    (doseq [stripe (range num-stripes)]
      (let [offset (* stripe-width stripe)
            color (nth colors stripe)]
        (q/fill color)
        (q/rect offset 0 (+ offset stripe-width) (q/height))))))

(q/defsketch random-stripes
  :title "Randomly colored stripes"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
