(ns generative-art.displaced-lines
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def line-count 20)
(def line-points 100)
(def max-seed 200000)

(defn displaced-line [{:keys [line-points]} factor]
  (q/stroke 0)
  (q/begin-shape)
  (let [width (/ (q/width) (dec line-points))]
    (doseq [x (range 0 (+ (q/width) width) width)]
      (q/vertex x (* 0.02 (q/random factor)))))
  (q/end-shape))

(defn setup []
  (q/frame-rate 30)
  {:line-count line-count
   :line-points line-points
   :seed (rand-int max-seed)})

(defn draw-state [{:keys [line-count seed] :as state}]
  (q/random-seed seed)
  (q/background 255)
  ;; draw tiled function
  (let [height (/ (q/height) line-count)]
    (doseq [y (range (/ height 2) (- (q/height) (/ height 2)) height)]
      (q/with-translation [0 y]
        (displaced-line state y)))))

(defn update-state [state]
  (if (q/key-pressed?)
    (case (q/key-as-keyword)
      :+ (update state :line-count inc)
      :- (-> state
             (update :line-count dec)
             (update :line-count max 1))
      :r (assoc state :seed (rand-int max-seed))
      :p (update state :line-points inc)
      :P (-> state
             (update :line-points dec)
             (update :line-points max 2))
      := (-> state
             (assoc :line-count line-count)
             (assoc :line-points line-points))
      state)
    state))

(q/defsketch displaced-lines
  :title "Displaced Lines"
  :size [640 640]
  :setup setup
  :draw draw-state
  :update update-state
  :middleware [m/fun-mode])
