(ns generative-art.perlin-two-dimensions
  (:require [quil.core :as q]))

(defn setup []
  (q/no-stroke))

(defn draw []
  (doseq [x (range 0 (q/width) 10)
          y (range 0 (q/height) 10)
          :let [color (int (* 255 (q/noise (* 0.01 x) (* 0.01 y))))]]
    (q/fill color)
    (q/rect x y 10 10)))

(q/defsketch perlin-two-dimensions
  :title "Two-dimensional Perlin Noise"
  :size [500 500]
  :setup setup
  :draw draw)
