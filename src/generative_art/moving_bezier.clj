(ns generative-art.moving-bezier
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 30)
  (q/stroke 0 18)
  (q/no-fill)
  (q/background 255)
  {:t 0})

(defn update-state [state]
  (update state :t #(+ % 0.005)))

(defn draw-state [{:keys [t]}]
  (when (= 0 (mod (q/frame-count) 500))
    (q/background 255))
  (let [x1 (* (q/width) (q/noise (+ t 15)))
        x2 (* (q/width) (q/noise (+ t 25)))
        x3 (* (q/width) (q/noise (+ t 35)))
        x4 (* (q/width) (q/noise (+ t 45)))
        y1 (* (q/width) (q/noise (+ t 55)))
        y2 (* (q/width) (q/noise (+ t 65)))
        y3 (* (q/width) (q/noise (+ t 75)))
        y4 (* (q/width) (q/noise (+ t 85)))]
    (q/bezier x1 y1 x2 y2 x3 y3 x4 y4)))

(q/defsketch moving-bezier
  :title "Moving Bezier Curve"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
