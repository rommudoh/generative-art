(ns generative-art.galaxy
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def diameter 500)
(def arms 2)
(def density 2) ;; smaller density = more widely spread stars, greater = more stars at the core
(def delta-turn 1.5)

(def star-count (* 40 diameter))
(def thickness (* 0.16 diameter))

(defn generate-star-positions []
  (for [arm (range arms)
        star (range (/ star-count arms))
        :let [dist (* (rand) (rand) diameter 0.5)
              height-sign (if (> (rand) 0.5) -1 1)
              height (if (> dist 0)
                       (* (rand) (rand) (rand) thickness (Math/exp (/ (- dist) diameter)) 0.5 height-sign)
                       (* (rand) thickness 0.5 height-sign))
              dr1 (* (apply * (repeatedly (inc density) rand)) q/PI -1)
              dr2 (* (apply * (repeatedly (inc density) rand)) q/PI)
              turn (+ (/ (* delta-turn q/PI (- diameter (* 2 dist))) diameter) dr1 dr2)
              px (* (q/cos (+ turn (/ (* arm 2 q/PI) arms))) dist)
              pz (* (q/sin (+ turn (/ (* arm 2 q/PI) arms))) dist)]]
    [px height pz]))

(defn setup []
  (q/frame-rate 30)
  {:stars (generate-star-positions)})

(defn draw-state [{:keys [stars]}]
  (q/background 0)
  (q/lights)
  (q/stroke 255)
  (doseq [star stars]
    (q/with-translation star
      (q/point 0 0 0))))

(q/defsketch galaxy
  :title "Galaxy"
  :size [1024 768]
  :setup setup
  :draw draw-state
  :renderer :opengl
  :navigation-3d {:position [(/ diameter -2) (/ diameter -2) 0]
                  :straight [1 1 0] }
  :middleware [m/fun-mode
               m/navigation-3d])
