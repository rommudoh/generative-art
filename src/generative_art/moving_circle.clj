(ns generative-art.moving-circle
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup []
  (q/frame-rate 30)
  (q/background 0)
  {:t 0
   :gr (q/create-graphics (q/width) (q/height))})

(defn update-state [state]
  (update state :t #(+ % 0.01)))

(defn draw-state [{:keys [t gr]}]
  ;; fade the background
  (q/with-graphics gr
    (q/background 0 5)
    (let [x (* (q/width) (q/noise t))
          y (* (q/height) (q/noise (+ t 5)))
          r (* 255 (q/noise (+ t 10)))
          g (* 255 (q/noise (+ t 15)))
          b (* 255 (q/noise (+ t 20)))]
      (q/no-stroke)
      (q/fill r g b)
      (q/ellipse x y 120 120)))
  (q/image gr 0 0))

(q/defsketch moving-circle
  :title "Moving Circle with fading Background"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
