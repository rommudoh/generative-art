(ns generative-art.random-walker
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def t-step 0.01)

(defn setup []
  (q/frame-rate 30)
  (q/background 255)
  {:walker {:x (q/map-range (q/noise 0) 0 1 0 (q/width))
            :y (q/map-range (q/noise 10000) 0 1 0 (q/height))
            :tx 0
            :ty 10000}})

(defn update-walker [{:keys [tx ty] :as walker}]
  (-> walker
      (assoc :x (q/map-range (q/noise tx) 0 1 0 (q/width)))
      (assoc :y (q/map-range (q/noise ty) 0 1 0 (q/height)))
      (update :tx + t-step)
      (update :ty + t-step)))

(defn update-state [state]
  (-> state
      (update :walker update-walker)))

(defn draw-walker [{:keys [x y]}]
  (q/stroke 0)
  (q/point x y))

(defn draw-state [{:keys [walker]}]
  (draw-walker walker))

(q/defsketch perlin-walker
  :title "Random Walker with Perlin Noise"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
