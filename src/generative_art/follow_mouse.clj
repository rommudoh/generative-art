(ns generative-art.follow-mouse
  (:require [quil.core :as q]))

(defn setup []
  (q/stroke 255)
  (q/background 192 64 0))

(defn draw []
  (let [x 150
        y 25]
    (q/line x y (q/mouse-x) (q/mouse-y))))

(q/defsketch follow-mouse
  :title "follow mouse position"
  :size [500 500]
  :setup setup
  :draw draw)
