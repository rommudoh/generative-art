(ns generative-art.game-of-life
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def cols 20)
(def rows 20)

(defn make-cells []
  (into {} (for [x (range cols)
                 y (range rows)]
             {[x y] (if (zero? (rand-int 5)) :alive :empty)})))

(defn setup []
  (q/frame-rate 2)
  {:cells (make-cells)})

(defn neighbor-cell [[[x1 y1] _] [[x2 y2] cell-state]]
  (and (not= [x1 y1] [x2 y2])
       (= cell-state :alive)
       (#{x1 (inc x1) (dec x1)} x2)
       (#{y1 (inc y1) (dec y1)} y2)))

(defn update-cell [cell cells]
  ;; rules:
  ;; living: 0-1 neighbors: die, 4+ neighbors: die, 2-3 neighbors: live
  ;; empty: 3 neighbors: new life
  (let [[[x y] cell-state] cell
        neighbors (count (filter #(neighbor-cell cell %) cells))]
    (case cell-state
      :alive (if (#{2 3} neighbors)
               [[x y] :alive]
               [[x y] :dead])
      (if (= 3 neighbors)
        [[x y] :alive]
        cell))))

(defn update-cells [cells]
  (into {} (map (fn [cell] (update-cell cell cells)) cells)))

(defn update-state [state]
  (update state :cells update-cells))

(defn draw-state [{:keys [cells]}]
  (q/background 255)
  (let [cell-width (/ (q/width) cols)
        cell-height (/ (q/height) cols)]
    (q/stroke 0)
    (q/stroke-weight 2)
    (doseq [cell cells
            :let [[[x y] cell-state] cell]]
      (case cell-state
        :alive (q/fill 0)
        :dead (q/fill 125)
        (q/no-fill))
      (q/rect (* x cell-width) (* y cell-height) cell-width cell-height))))

(q/defsketch game-of-life
  :title "Game of Life"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
