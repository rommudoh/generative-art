(ns generative-art.bouncing-ball
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(def ball-count 20)
(def max-velocity 5)
(def min-radius 20)
(def max-radius 50)

(defn setup []
  {:balls (repeatedly ball-count (fn [] {:radius (+ min-radius (rand-int (- max-radius min-radius)))
                                         :color (q/color (rand-int 255) (rand-int 255) (rand-int 255))
                                         :x (rand-int (q/width))
                                         :y (rand-int (q/height))
                                         :dx (inc (rand-int max-velocity))
                                         :dy (inc (rand-int max-velocity))}))})

(defn draw-ball [{:keys [radius color x y]}]
  (q/no-stroke)
  (q/fill color)
  (q/ellipse x y radius radius))

(defn draw-state [{:keys [balls]}]
  (q/background 120)
  (doseq [ball balls]
    (draw-ball ball)))

(defn move-ball [{:keys [dx dy radius] :as ball}]
  (-> ball
      (update :x (fn [x] (cond
                           (< (+ x dx (- (/ radius 2))) 0) (/ radius 2)
                           (> (+ x dx (/ radius 2)) (q/width)) (- (q/width) (/ radius 2))
                           :else (+ x dx))))
      (update :y (fn [y] (cond
                           (< (+ y dy (- (/ radius 2))) 0) (/ radius 2)
                           (> (+ y dy (/ radius 2)) (q/height)) (- (q/height) (/ radius 2))
                           :else (+ y dy))))))

(defn update-ball [{:keys [x y radius] :as ball}]
  (-> ball
      (update :dx (fn [dx] (if (or (< (+ x dx (- (/ radius 2))) 0) (> (+ x dx (/ radius 2)) (q/width))) (- dx) dx)))
      (update :dy (fn [dy] (if (or (< (+ y dy (- (/ radius 2))) 0) (> (+ y dy (/ radius 2)) (q/height))) (- dy) dy)))
      (move-ball)))

(defn square [x]
  (* x x))

(defn handle-collision [{:keys [x y radius] :as ball} balls]
  (let [colliding-balls (filter
                         (fn [{x1 :x y1 :y radius1 :radius}]
                           (< (Math/sqrt (+ (square (- x x1)) (square (- y y1))))
                              (/ (+ radius radius1) 2)))
                         balls)]
    (if (= (count colliding-balls) 1)
      ball
      (-> ball
          (update :dx -)
          (update :dy -)))))

(defn update-state [state]
  (-> state
      (update :balls (fn [balls] (map #(handle-collision % balls) balls)))
      (update :balls #(map update-ball %))))

(q/defsketch bouncing-ball
  :title "Bouncing Ball"
  :size [500 500]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
