(ns generative-art.branching
  (:require [quil.core :as q]))

(def max-depth 5)
(def line-multiplier 3)

(defn spawn-new-line [{:keys [x y alpha length depth color]}]
  (let [beta (+ alpha (* (q/random-gaussian) (/ q/QUARTER-PI 3)))
        x' (+ x (* length (q/sin beta)))
        y' (+ y (* length (q/cos beta)))]
    (q/stroke (q/lerp-color 0 color (/ 1 (inc depth))))
    (q/line x y x' y')
    {:x x' :y y'
     :alpha beta
     :length (* length 0.75)
     :depth (dec depth)
     :color color}))

(defn branching-line [line-endings]
  (when-not (empty? line-endings)
    (let [new-line-endings
          (reduce (fn [acc line-ending]
                    (into acc (repeatedly line-multiplier
                                          #(spawn-new-line line-ending))))
                  []
                  line-endings)]
      (recur (filter (comp not zero? :depth) new-line-endings)))))

(defn draw []
  (q/no-loop)
  (q/background 255)
  (q/stroke 0)
  (q/stroke-weight 2)
  (let [starting-points [{:x (* (q/width) 0.5)
                          :y (q/height)
                          :alpha 0
                          :length (* (q/height) -0.3)
                          :depth max-depth
                          :color (q/color 255 0 0)}
                         {:x (* (q/width) 0.25)
                          :y (q/height)
                          :alpha 0
                          :length (* (q/height) -0.2)
                          :depth (dec max-depth)
                          :color (q/color 0 255 0)}
                         {:x (* (q/width) 0.75)
                          :y (q/height)
                          :alpha 0
                          :length (* (q/height) -0.1)
                          :depth (- max-depth 2)
                          :color (q/color 0 0 255)}]]
    (branching-line starting-points)))

(q/defsketch branching
  :title "Branching"
  :size [500 500]
  :draw draw)
