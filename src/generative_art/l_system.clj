(ns generative-art.l-system
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn deg->rad [x]
  (/ (* q/PI x) 180))

(def new-turtle {:x 0 :y 0 :alpha 0 :stack '()})

;; turtle instructions
(defn forward [draw-line? {:keys [x y alpha] :as turtle}]
  (let [x' (+ x (q/cos alpha))
        y' (+ y (q/sin alpha))]
    (when draw-line?
      (q/line x y x' y'))
    (assoc turtle :x x' :y y')))

(defn turn [delta turtle]
  (update turtle :alpha + delta))

(defn push-state [turtle]
  (update turtle :stack #(conj % (dissoc turtle :stack))))

(defn pop-state [{:keys [stack] :as turtle}]
  (-> turtle
      (merge (first stack))
      (assoc :stack (rest stack))))

;; L-system for Koch Curve
(def koch-curve
  {:start [:F]
   :rules {:F '(:F :+ :F :- :- :F :+ :F)}
   :instructions {:F (partial forward true)
                  :+ (partial turn (deg->rad -60))
                  :- (partial turn (deg->rad 60))}
   :length-reduction 3
   :max-iteration 5
   :alignment :bottom})

;; L-system for different Koch Curve (90°)
(def koch-curve-2
  {:start [:F]
   :rules {:F '(:F :+ :F :- :F :- :F :+ :F)}
   :instructions {:F (partial forward true)
                  :+ (partial turn (deg->rad -90))
                  :- (partial turn (deg->rad 90))}
   :length-reduction 3
   :max-iteration 5
   :alignment :bottom})

(def sierpinski-triangle
  {:start [:F :- :G :- :G]
   :rules {:F [:F :- :G :+ :F :+ :G :- :F]
           :G [:G :G]}
   :instructions {:F (partial forward true)
                  :G (partial forward true)
                  :+ (partial turn (deg->rad 120))
                  :- (partial turn (deg->rad -120))}
   :length-reduction 2
   :max-iteration 8
   :alignment :bottom})

(def sierpinski-2
  {:start [:A]
   :rules {:A [:B :- :A :- :B]
           :B [:A :+ :B :+ :A]}
   :instructions {:A (partial forward true)
                  :B (partial forward true)
                  :+ (partial turn (deg->rad -60))
                  :- (partial turn (deg->rad 60))}
   :length-reduction 2
   :max-iteration 8
   :alignment :bottom})

(def binary-tree
  {:start [0]
   :rules {0 [1 :push :- 0 :pop :+ 0]
           1 [1 1]}
   :instructions {0 (partial forward true)
                  1 (partial forward true)
                  :push (partial push-state)
                  :pop (partial pop-state)
                  :+ (partial turn (deg->rad 45))
                  :- (partial turn (deg->rad -45))}
   :length-reduction 2
   :max-iteration 8
   :alignment :middle})

(def branching
  {:start [:A]
   :rules {:A [:F :- :push :push :A :pop :+ :A :pop :+ :F :push :+ :F :A :pop :- :A]
           :F [:F :F]}
   :instructions {:A (partial forward true)
                  :F (partial forward true)
                  :push (partial push-state)
                  :pop (partial pop-state)
                  :+ (partial turn (deg->rad 20))
                  :- (partial turn (deg->rad -20))}
   :length-reduction 2.5
   :max-iteration 7
   :alignment :middle})

(def stochastic
  {:start [:F]
   :rules {:F [{:probability 0.33
                :result [:F :push :+ :F :pop :F :push :- :F :pop :F]}
               {:probability 0.33
                :result [:F :push :+ :F :pop :F]}
               {:probability 0.33
                :result [:F :push :- :F :pop :F]}]}
   :instructions {:F (partial forward true)
                  :push (partial push-state)
                  :pop (partial pop-state)
                  :+ (partial turn (deg->rad 30))
                  :- (partial turn (deg->rad -30))}
   :length-reduction 2.4
   :max-iteration 7
   :alignment :middle})


;; choose rule if probability
(defn choose-rule [rule]
  (if (map? (first rule))
    (let [probability-sum (reduce #(+ %1 (:probability %2)) 0 rule)
          random-value (rand probability-sum)]
      (loop [[{:keys [probability result]} & rest] rule
             sum 0]
        (if (< random-value (+ sum probability))
          result
          (recur rest (+ sum probability)))))
    rule))

;; generate next state
(defn apply-rules [in rules]
  (mapcat (fn [x]
            (if (some #{x} (keys rules))
              (choose-rule (get rules x))
              (list x)))
          in))

;; generate lazy seq of all states
(defn expand [coll rules]
  (lazy-seq (cons coll (expand (apply-rules coll rules) rules))))

(defn init-system [{:keys [start rules] :as l-system}]
  (let [result (expand start rules)]
    (assoc l-system :result result :iteration 0)))

;; setup Quil state
(defn setup []
  (q/frame-rate 30)
  (init-system koch-curve))

;; apply turtle instructions to draw
(defn draw-state [{:keys [instructions result alignment
                          iteration length-reduction]}]
  (q/background 230)
  (q/fill 0)
  (q/text-size 20)
  (q/text (str "Iteration: " iteration) 20 30)
  (case alignment
    :bottom (q/translate 0 (- (q/height) 20))
    :top (q/translate 0 20)
    :middle (q/translate 0 (/ (q/height) 2))
    nil)
  (q/scale (/ (q/width) (Math/pow length-reduction iteration))
           (/ (q/height) (Math/pow length-reduction iteration)))
  (q/stroke-weight (/ (Math/pow length-reduction iteration) (q/height)))
  (->> (nth result iteration)
       (map instructions)
       (reduce (fn [state f] (f state)) new-turtle)))

;; increment the iteration
(defn update-state [{:keys [max-iteration] :as state}]
  (if (q/key-pressed?)
    (case (q/key-as-keyword)
      :1 (init-system koch-curve)
      :2 (init-system koch-curve-2)
      :3 (init-system sierpinski-triangle)
      :4 (init-system sierpinski-2)
      :5 (init-system binary-tree)
      :6 (init-system branching)
      :7 (init-system stochastic)
      :r (init-system state)
      state)
    (if (zero? (mod (q/frame-count) 30))
      (-> state
          (update :iteration #(mod (inc %) (inc max-iteration))))
      state)))

;; sketch definition
(q/defsketch l-system
  :title "L System"
  :size [500 500]
  :setup setup
  :draw draw-state
  :update update-state
  :middleware [m/fun-mode])
