(ns generative-art.rectangles
  (:require [quil.core :as q]))

(def rect-count 6)
(def sub-rect-count 3)

(defn draw []
  (q/no-loop)
  (q/background 255)
  (q/stroke 0)
  (q/stroke-weight 2)
  (let [x-offset (/ (q/width) (inc (* 3 rect-count)))
        y-offset (/ (q/height) (inc (* 3 rect-count)))
        width (* 2 x-offset)
        height (* 2 y-offset)
        color-1 (q/color (rand-int 255) (rand-int 255) (rand-int 255))
        color-2 (q/color (rand-int 255) (rand-int 255) (rand-int 255))]
    (doseq [i (range rect-count)
            j (range rect-count)
            k (range sub-rect-count)]
      (if (zero? k)
        (q/fill (q/lerp-color color-1 color-2 (/ (+ i j) (dec (* rect-count 2)))))
        (q/no-fill))
      (let [x (+ (* (inc i) width) (* i x-offset))
            y (+ (* (inc j) height) (* j y-offset))]
        (q/with-translation [x y]
          (q/scale (/ (+ k (inc i)) rect-count))
          (q/rotate (q/radians (* j (+ (* rect-count (rand)) 3))))
          (q/rect (- (/ width 2)) (- (/ height 2)) width height))))))

(q/defsketch rectangles
  :title "Rectangles"
  :size [500 500]
  :draw draw)
